from django.urls import path
from . import views as users_views

app_name = "users"


urlpatterns = [
    path(
        "all_users/",
        users_views.CustomUserListView.as_view(),
        name="all_users",
    ),
]
