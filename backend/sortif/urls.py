from django.contrib import admin
from django.urls import path, include
from users import views as users_views

urlpatterns = [
    path("admin/", admin.site.urls),
    path(
        "api-auth/",
        include("rest_framework.urls"),
    ),
    path(
        "api/v1/users/",
        include("users.urls"),
    ),
    # path("stripe/", include("djstripe.urls", namespace="djstripe")),
    # path('accounts/', include('allauth.urls')),
    path(
        "api/v1/register_account/",
        users_views.CustomUserCreateView.as_view(),
        name="register_account",
    ),
    path(
        "api/v1/login/",
        users_views.LoginAPIView.as_view(),
        name="account_login",
    ),
    path(
        "api/v1/logout/",
        users_views.LogoutAPIView.as_view(),
        name="account_logout",
    ),
    path(
        "api/v1/delete_account/",
        users_views.CustomUserDeletelView.as_view(),
        name="delete_account",
    ),
    path(
        "api/v1/change_password/",
        users_views.ChangePasswordAPIView.as_view(),
        name="password_change",
    ),
]
