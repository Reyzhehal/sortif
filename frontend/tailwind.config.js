module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './src/renderer/index.ejs'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        red: '#FF0000',
        blue: '#0000FF',
        green: '#122C32',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
